package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;

    @Before
    public void setup() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);
    }

    @Test
    public void testGetSaleOrders() {
        List<SaleTransaction> mockTransactions1 = new ArrayList<>();
        List<SaleTransaction> mockTransactions2 = new ArrayList<>();
        mockTransactions1.add(new SaleTransaction("T111", new SaleOrder("S111", mockTransactions1),
                new Product("P111", "Banana", "Very sweet", "banana.jpg", 100.0), 12));
        mockTransactions1.add(new SaleTransaction("T222", new SaleOrder("S111", mockTransactions1),
                new Product("P222", "Melon", "Very sweet", "xxx2", 50.0), 10));
        mockTransactions2.add(new SaleTransaction("T333", new SaleOrder("S222", mockTransactions2),
                new Product("P333", "Mango", "Very big", "mango.jpg", 10.0), 10));
        mockTransactions2.add(new SaleTransaction("T444", new SaleOrder("S222", mockTransactions2),
                new Product("P444", "Orange", "Vitamin C", "orange.jpg", 100.0), 10));

        List<SaleOrder> mockOrders = new ArrayList<>();
        mockOrders.add(new SaleOrder("S111", mockTransactions1));
        mockOrders.add(new SaleOrder("S222", mockTransactions2));
        when(orderDao.getOrders()).thenReturn(mockOrders);
        assertThat(saleOrderService.getSaleOrders(), hasItems(new SaleOrder("S111", mockTransactions1),
                new SaleOrder("S222", mockTransactions2)));
        assertThat(saleOrderService.getSaleOrders().size(),is(2));
    }

    @Test
    public void testGetAverageSaleOrderPrice() {
        List<SaleTransaction> mockTransactions1 = new ArrayList<>();
        List<SaleTransaction> mockTransactions2 = new ArrayList<>();
        mockTransactions1.add(new SaleTransaction("T111", new SaleOrder("S111", mockTransactions1),
                new Product("P111", "Banana", "Very sweet", "banana.jpg", 100.0), 12));
        mockTransactions1.add(new SaleTransaction("T222", new SaleOrder("S111", mockTransactions1),
                new Product("P222", "Melon", "Very sweet", "xxx2", 50.0), 10));
        mockTransactions2.add(new SaleTransaction("T333", new SaleOrder("S222", mockTransactions2),
                new Product("P333", "Mango", "Very big", "mango.jpg", 10.0), 10));
        mockTransactions2.add(new SaleTransaction("T444", new SaleOrder("S222", mockTransactions2),
                new Product("P444", "Orange", "Vitamin C", "orange.jpg", 100.0), 10));

        List<SaleOrder> mockOrders = new ArrayList<>();
        mockOrders.add(new SaleOrder("S111", mockTransactions1));
        mockOrders.add(new SaleOrder("S222", mockTransactions2));
        when(orderDao.getOrders()).thenReturn(mockOrders);
        assertThat(saleOrderService.getAverageSaleOrderPrice(), is(1400.0));
    }
}
