package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.dao.UserDaoImpl;
import camt.se234.project.entity.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class AuthenticationServiceImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticationService;

    @Before
    public void setup() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }

    @Test
    public void testAuthenicateWithMock() {
        when(userDao.getUser("lonewolf", "cmucamt60")).thenReturn(new User("lonewolf", "cmucamt60", "student"));
        assertThat(authenticationService.authenticate("lonewolf", "cmucamt60"), is(new User("lonewolf", "cmucamt60", "student")));
        assertThat(authenticationService.authenticate("lonewolf", "cmucamt60"), is(notNullValue()));

    }
}
