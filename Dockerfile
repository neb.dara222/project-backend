FROM openjdk:8-jdk-alpine
ARG JAVA_FILE
COPY ${JAVA_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","app.jar","--imageServer=http://34.227.115.36:8086/images/"]
